Angular Frontend Applikation für Weihnachtskrippe
=================================================

Dieses Git-Repository enthält alle notwendigen Quellcodes zur Erstellung der
Angular Web-Applikation für eine Weihnachtskrippensteuerung auf Basis eines
Raspberry PI Einplatinen-Computers.

Container Image laden und starten
---------------------------------

Das fertige Docker-Image zum Herunterladen aus dem Docker-Registry finden Sie
hier:

```
docker pull registry.gitlab.com/tkautenburger/crib-control/crib-arm:latest
```

Nach Download des Docker-Image können Sie die Web-Applikation auf einem
Raspberry PI mit Hypriotos Betriebssystem folgendermaßen starten:

```
docker run -d --name crib -p 8080:8080 \
  --restart always \
  registry.gitlab.com/tkautenburger/crib-control/crib-arm:latest
```
Danach ist die Applikation über Web-Browser unter der folgenden URL erreichbar:

```
http://<my-host-ip>:8080/
```
wobei <my-host-ip> der Hostname oder die IP-Adresse des Raspberry PI in Ihrem LAN ist. Wenn alles gut gegangen ist
sollten Sie die folgende Ansicht auf Ihrem Browser sehen:

![Krippen-GUI](crib-control.png)

Wenn Sie nur die Konfigurationskarte sehen, kann die Applikation Ihren GPIO-Controller noch nicht finden. Lesen Sie hierzu den
nächsten Abschnitt.

Die Angular Web-Applikation sucht den GPIO Controller sowie den MQTT-Broker initial unter dem Hostnamen `crib-pi`. 
Für den GPIO Controller wird der Port `5556` erwartet und MQTT via WebSockets nutzt den Port `9001`.
Diesen Namen müssen Sie entweder auf Ihrem DNS-Server (z.B. FritzBox) der IP-Adresse Ihres Raspberry zuordnen. Ansonsten funktioniert die Kommunikation nicht
und die Oberfläche der Applikation wird nicht vollständig angezeigt. Können oder möchten Sie nicht einen DNS-Eintrag auf ihrem Router machen, haben Sie alternativ
die Möglichkeit in der Weboberfläche der App den Hostnamen oder die IP-Adresse und den Kommunikationsport zum GPIO Controller einzugeben und zu setzen. Dieser wird
dann für 30 Tage als Browser-Cookie gespeichert. Der MQTT Server wird jeweils immer auf dem gleichen Host erwartet, auf dem auch der GPIO Controller läuft. 
Es sei angemerkt, dass Mobiltelefone, wie z.B. iPhones, oftmals ein Problem mit einem lokalen DNS Server haben. 
Auch wenn dieser als DNS-Resolver in den Netzwerkeinstellungen des Mobiltelefons auftaucht,
kann es trotzdem sein, dass die DNS-Namensauflösung nicht funktioniert. Dann hilft nur die Einstellung über die Weboberfläche der Applikation oder die 
Angular-Applikation erneut zu kompilieren und statt dem Hostnamen die IP-Adresse Ihres Raspberry in der Umgebungseinstellung `environment.xxx.ts`ihrer 
Angular Applikation einzutragen.

Container Image selbst bauen
----------------------------
Wer sich lieber sein eigenes Docker-Image basteln will, dem sei der beigefügte Dockerfile im Repository ans Herz gelegt. Das Image kann wie folgt erzeugt werden:
```
docker build -t <my-image-path> -f Dockerfile .
```
Das Kommando muss im gleichen Verzeichnis ausgeführt werden, in dem sich `Dockerfile`, `nginx.conf` und das `dist` Unterverzeichnis mit allen ausführbaren Javascripts der Angular-Applikation befinden.
Auf eine Erklärung, wie man eine Angular-Applikation compiliert, sei hier verzichtet und es wird auf die einschlägige Online-Literatur verwiesen. 

Die `nginx.conf` Konfigurationsdatei ist denkbar einfach gehalten und hat den folgenden Inhalt:
```
events {}
http {
  server {
    listen 8080;
    server_name base;

    # Redirect to index if file not found

    location / {
      root /usr/share/nginx/html;
      index index.html index.htm;
      try_files $uri$args $uri$args/ /index.html;
    }

    # Additional configurations go here
  }
}
```
Der NGINX Web-Server lauscht also auf Port 8080 und lädt die Angular-Applikation, welche vorher über den Dockerfile in das entsprechende Verzeichnis geschrieben wurde.  

Viel Spaß,
Thomas Kautenburger
