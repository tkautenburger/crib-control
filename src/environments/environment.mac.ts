export const environment = {
  production: true,
  host: 'crib-pi',
  restPort: 5556,
  port: 9001
};
