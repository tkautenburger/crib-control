import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { environment } from '../environments/environment';
import { CookieService } from 'ngx-cookie-service';

import {
  MatCardModule,
  MatSliderModule,
  MatInputModule,
  MatButtonModule,
  MatSlideToggleModule} from '@angular/material';

import {
  MqttModule,
  MqttService,
  IMqttServiceOptions
} from 'ngx-mqtt';


import { AppComponent } from './app.component';
import { SwitchComponent } from './switch/switch.component';

// Initial MQTT configuration
export const MQTT_SERVICE_OPTIONS: IMqttServiceOptions = {
  hostname: environment.host,
  port: environment.port,
  protocol: 'ws'
};

export function mqttServiceFactory() {
  return new MqttService(MQTT_SERVICE_OPTIONS);
}

@NgModule({
  declarations: [
    AppComponent,
    SwitchComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatCardModule,
    MatInputModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatButtonModule,
    MqttModule.forRoot({
      provide: MqttService,
      useFactory: mqttServiceFactory
    })
 ],
  providers: [ CookieService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
