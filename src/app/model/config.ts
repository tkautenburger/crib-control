export class Control {
  name: String;
  pin: number;
  status: number;
}

export class Config {
  title: String;
  controls: Control[];
}
