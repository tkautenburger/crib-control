import { Component, OnDestroy, OnInit, EventEmitter, Output } from '@angular/core';
import { NgForm, NgControl } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { CookieService } from 'ngx-cookie-service';
import { MqttService, IMqttMessage, IMqttServiceOptions } from 'ngx-mqtt';
import { Observable } from 'rxjs/Observable';

import { Status } from '../model/status';
import { Config } from '../model/config';
import { Control } from '../model/config';
import { LightService } from '../services/light.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.css']
})

export class SwitchComponent implements OnInit {
  @Output() notify: EventEmitter<string> = new EventEmitter<string>();

  mqttMessage: string;
  serverAddress = 'crib-pi:5556';

  // Button toggle
  checkedButton: boolean[] = [];
  // Button Lables
  rooms: String[] = [];
  // Button GPIO numbers
  pins: number[] = [];
  // Application title
  title: string;
  // Some general helper variables
  errMsg: string;
  status: Status;
  config: Config;

  constructor(private _mqttService: MqttService, private titleService: Title,
    private cookieService: CookieService, private lightService: LightService) { }

  ngOnInit() {
    if (!this.cookieService.check('gpioControllerAddress')) {
      this.setCookie();
    }
    this.serverAddress = this.cookieService.get('gpioControllerAddress');
    // observe on mqtt topics
    this.mqttListen();
    // read config data from gpio service
    this.readConfig();
  }

    // Toggle button state and send request to gpio controller
    onActiveSwitch(i: number) {
    this.checkedButton[i] = !this.checkedButton[i];
    if (this.checkedButton[i] === true) {
      this.lightService.setRelativeUrl('/gpioHigh');
    } else {
      this.lightService.setRelativeUrl('/gpioLow');
    }
    this.lightService.query<Status>({ pin: this.pins[i] })
      .subscribe(status => {
        this.status = status;
      },
        error => {
          this.errMsg = 'Error in HTTP GET: ' + error;
          console.log('ERROR', this.errMsg);
      });
  }

  // change the application title based on the configured value
  sendTitle(): void {
    this.notify.emit(this.title);
  }

  // set cookie with gpio controller address (for 30 days) and reconfigure and
  // reconnect to MQTT service to reflect server hostname / ip address change
  setCookie(): void {
    const expDate = new Date(Date.now() + (30 * 24 * 60 * 60 * 1000));
    this.cookieService.set('gpioControllerAddress', this.serverAddress, expDate);
    this.serverAddress = this.cookieService.get('gpioControllerAddress');
    const fields = this.serverAddress.split(':');
    // Set the new MQTT server parameters
    const MQTT_SERVICE_OPTIONS: IMqttServiceOptions = {
      hostname: fields[0],
      port: environment.port,
      protocol: 'ws'
    };
    // Reconnect to MQTT service (host might have changed)
    this._mqttService.disconnect();
    this._mqttService.connect(MQTT_SERVICE_OPTIONS);
    this.mqttListen();
    this.readConfig();
  }

  // Configure mqtt topics to observe
  mqttListen(): void {
    // Listen to lightOff messages
    this._mqttService.observe('gpc/lightOff')
      .subscribe((message) => {
        this.mqttMessage = message.payload.toString();
        const pin = parseInt(this.mqttMessage, 10);
        const i = this.config.controls.findIndex(element => element.pin === pin);
        if (i !== -1 && i < this.checkedButton.length) {
          this.checkedButton[i] = false;
        }
      });
    // Listen to lightOn messages
    this._mqttService.observe('gpc/lightOn')
      .subscribe((message) => {
        this.mqttMessage = message.payload.toString();
        const pin = parseInt(this.mqttMessage, 10);
        const i = this.config.controls.findIndex(element => element.pin === pin);
        if (i !== -1 && i < this.checkedButton.length) {
          this.checkedButton[i] = true;
        }
       });
  }

  // Read new configuration from gpio controller
  readConfig(): void {
    this.lightService.setHost(this.serverAddress);
    this.lightService.setRelativeUrl('/gpioConfig');
    this.lightService.get<Config>('').subscribe(config => {
      this.config = config;
      this.pins.length = 0;
      this.rooms.length = 0;
      this.checkedButton.length = 0;
      for (const element of this.config.controls) {
        this.pins.push(element.pin);
        this.rooms.push(element.name);
        this.checkedButton.push(element.status === 1);
      }
      this.title = config.title.toString();
      this.titleService.setTitle(this.title);
      this.notify.emit(this.title);
    });
  }
}
