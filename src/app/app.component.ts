import { Component } from '@angular/core';
import { LightService } from './services/light.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    LightService
  ]
})
export class AppComponent {
  title = 'Weihnachtskrippe';

  // Get application title from child component
  getChildTitle(title: string): void {
    this.title = title;
  }

}
