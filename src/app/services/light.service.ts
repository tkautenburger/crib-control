import { Injectable } from '@angular/core';
import { RestService } from './rest.service';

@Injectable()
export class LightService extends RestService {
  resource = '/';

  setHost(host: string) {
    this.host = host;
    this.base = 'http://' + this.host;
  }

  setRelativeUrl(url: string) {
    this.resource = url;
  }

}
