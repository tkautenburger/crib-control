import { Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

interface Query {
  pin?: number;
}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json;charset=UTF-8'
  })
};

export class RestService {
  host = environment.host + ':' + environment.restPort;
  base = 'http://' + this.host;
  resource = '/';
  type: any;

  constructor(@Inject(HttpClient) private http: HttpClient) { }

  get url() {
    return this.base + this.resource;
  }

  query<T>(query?: Query) {
    let url = this.url;
    if (query) {
      url += `?${this.toQueryString(query)}`;
    }
    return this.http.get<T>(url, httpOptions);
  }

  get<T>(id: string) {
    return this.http.get<T>(this.url + '/' + id, httpOptions);
  }

  count<T>(id: string) {
    return this.http.get<T>(this.url + '/' + id, httpOptions);
  }

  create<T>(body: any) {
    return this.http.post<T>(this.url, body, httpOptions);
  }

  update<T>(id: string, body: any) {
    return this.http.put<T>(this.url + '/' + id, body, httpOptions);
  }

  delete<T>(id: string) {
    return this.http.delete<T>(this.url + '/' + id, httpOptions);
  }

  deleteBody<T>(id: string, body: any) {
    return this.http.request<T>('DELETE', this.url + '/' + id, {body: body, headers: httpOptions.headers});
  }

  private toQueryString(paramsObject) {
    return Object
      .keys(paramsObject)
      .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(paramsObject[key])}`)
      .join('&');
  }
}
